# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# ### Restart ColdFusion


# !ansible hospitaldevWin -m win_service -a 'name="ColdFusion 2018 Application Server" state=restarted'

# ### Restart Apache

# !ansible hospitaldevLin -m service -a 'name=httpd state=restarted'

# ### Clear logs

# !ansible hospitaldevLin -m shell -a 'truncate -s 0 /var/log/chartaccess/medgenix_setup/development.log'

# ### Update fdb.jar

# !ansible hospitaldevWin -m win_shell -a "robocopy D:\ChartAccess_System\GIT_Clones\chartaccess\ChartAccess\conf\bin\lib D:\ChartAccess_System\GIT_Clones\chartaccess\ChartAccess\coldfusion\application\bin\lib prognosis.chartaccess.fdb.jar"

# ### Get deployed ticket Win

# !ansible hospitaldevWin -m win_shell -a 'cd D:/ChartAccess_System/GIT_Clones/chartaccess; git log --oneline origin/release/rc2.0.. | findstr /c:"Merge remote-tracking branch '\''o" | %{$_.Split(" ")[4]}'

# ### Get deployed ticket Lin

# !ansible hospitaldevLin -m shell -a "cd /inetpub/git/chartaccess && git log  --oneline origin/release/rc2.0.. | grep \"Merge remote-tracking branch 'origin\" | awk '{print \$5}'"

# ### Build nursing board

# !ansible hospitaldevLin -m shell -a "cd /inetpub/git/chartaccess/nursingboard && npm run build"

# ### Update skipped tickets

# !scp skip.txt hos:/tmp/skip.txt && scp skip.txt hosapp:/d/ChartAccess_System/
