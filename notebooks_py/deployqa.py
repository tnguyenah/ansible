# ---
# jupyter:
#   jupytext:
#     formats: notebooks//ipynb,notebooks_py//py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] hidePrompt=true hideCode=true
# # Prerequisite 
# You need a jira api key to use this tool. You can grab one [here](https://id.atlassian.com/manage/api-tokens). Notes: Save your api key. This step needs to be done just once.

# + tags=["hide_input"] hidePrompt=true hideCode=true jupyter={"source_hidden": true}
import ipywidgets as widgets
from jira import JIRA
from IPython.display import Markdown

def on_test_click(change):
    jira = JIRA({'server': 'https://simplifymd.atlassian.net'}, basic_auth=(email.value, api_key.value), max_retries=0)
    projects = jira.projects()
    label.value = 'Looks like not correct!' if len(projects) == 0 else 'There you go!'

    
email = widgets.Text(description='Your email')
api_key = widgets.Password(description='API key:')
test_button = widgets.Button(description='Test', button_style='info')
label = widgets.Label()
test_button.on_click(on_test_click)

display(Markdown('___'))
display(email)
display(api_key)
display(widgets.HBox([test_button, label]))
display(Markdown('___'))

# + tags=["hide_input"] hidePrompt=true hideCode=true jupyter={"source_hidden": true}
from ipywidgets import *
from pprint import pprint
from IPython.display import Markdown


def hide_all_mode_options():
    mode_options = mode.children[1]
    for i in range(len(mode_options.children)):
        mode_options.children[i].layout.display = 'none'

def on_server_change(change):
    if change.new in ['hospitaldev', 'hospitaldev2', 'hospitalqa']:
        more_options.value = []
    else:
        more_options.value = ['--dry-run', '--no-comment', '--verbose']

def on_mode_change(change):
    hide_all_mode_options()
    mode_options = mode.children[1]

    if change.new == 'flexible':
        mode_options.children[0].layout.display = ''
    elif change.new == 'only':
        mode_options.children[1].layout.display = ''

def on_run_click(change):
    try:
        if not email.value or not api_key.value: raise NameError
        dl_info_label.value = 'Running...'

        parameters = f'--email {email.value} --api-key {api_key.value} '

        if mode.children[0].value == 'flexible':
            parameters += f'--ticket-list {mode.children[1].children[0].children[0].value} --max-issue {mode.children[1].children[0].children[2].value} '
        elif mode.children[0].value == 'only':
            parameters += f'--only {mode.children[1].children[1].children[0].value} '

        parameters += ' '.join(more_options.value)

        # Call ansible
#         !ansible-playbook ../playbooks/deployqa.yml -e target={servers.value} -e "params='{parameters}'"
        dl_info_label.value = 'Done'
    except NameError:
        print('Please enter you email and api key')
        pass


# define some elements
servers = widgets.RadioButtons(description="Pick a server", options=['hospitaldev', 'hospitaldev2', 'hospitalqa','josh', 'gavin', 'ridong', 'tuan'])
mode = VBox([
#     mode type
    widgets.RadioButtons(options=["flexible", "only"]),
#     mode options
    widgets.VBox([
        HBox([Text(layout=Layout(width='200px')), Label(value="+"), BoundedIntText(value=10, min=0, max=100, step=1, layout=Layout(width='50px')), Label(value="tickets")]),
        HBox(children=[Text(layout=Layout(width='200px'))], layout=Layout(display='None'))
    ])
])

more_options = widgets.SelectMultiple(
    description='Optional',
    options=[('no change of ticket status', '--dry-run'), ('no comment on tickets', '--no-comment'), ('show detail log', '--verbose')])
dl_button = widgets.Button(description='Run', button_style='info')
dl_info_label = widgets.Label()
container = widgets.HBox([servers, mode, more_options])

# set event handlers
servers.observe(on_server_change, names='value')
mode.children[0].observe(on_mode_change, names='value')
dl_button.on_click(on_run_click)


# show the UI
display(Markdown('___'))
display(Markdown('### Deploy code'))
display(container)
display(widgets.VBox([dl_button, dl_info_label]))
display(HTML('<a href="skip.txt" target="_blank">Skip List</a>'))
display(Markdown('___'))

