# Ansible Utilities

Some utilities using Ansible

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
 - python3: ```sh scripts/linux/install_python3.sh```
 - sshpass: ```yum install sshpass -y```
---
Optional:
There will be error when run **pip install -r requirements.txt**, you can skip those errors if you don't need below packages
- Related to Pyodbc
```$xslt
How to install pyodbc
```

- Related to Jupyter Notebook, need to open port 8888 (or any other ports)
```
How to open port 8888
```

- Related to deployqa.py, need to install python3 on Windows machine
```
# Install by chocolatey
choco install -a python --version=3.7.2
```

### Installing
```
# clone the source
git clone git@bitbucket.org:tnguyenah/ansible.git && cd ansible

# create a virtual environment
python3 -m venv env

# upgrade pip
pip install --upgrade pip

# activate virtual env
. env/bin/activate

pip install -r requirements.txt
sh init.sh
```


## Running the tests

Need to be in virtual env to run commands
```
. env/bin/activate
```

####  - Ping hospitaldev

```
ansible hospitaldev -m ping
```

#### Print out the current directory using shell module
```
ansible hospitaldev -m shell -a "pwd"
```

#### Restart apache
```
ansible tuan -m shell -a "service httpd restart"
```

#### Execute a script

Look at this

```
ansible-playbook playbooks/example.yml
```

Deploy to qa
```
ansible-playbook playbooks/deployqa.yml -v -e target=[YOUR_SERVER] -e "params=' --email [YOUR_EMAIL] --api-key [YOUR_API_KEY] --dry-run --no-comment'"
```
## Deployment

- Start Jupyter Notebook
```
jupyter lab --allow-root [--port=8888]
```

## Built With

* [Ansible](https://docs.ansible.com/ansible/latest/index.html) - An IT automation tool

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* [Tuan Nguyen](https://bitbucket.org/tnguyenah)

See also the list of [contributors](https://bitbucket.org/tnguyenah/ansible) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
