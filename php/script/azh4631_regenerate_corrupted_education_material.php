<?php

require '../vendor/autoload.php';

use Medoo\Medoo;

class AZH4631 {
    const PDF_DIR = '/inetpub/git/chartaccess/education_materials/education_materials/public/handoutsPdfFiles/';

    public $database;
    public function connect() {
        $database = new Medoo([
            'database_type' => 'mysql',
            'database_name' => 'prod_education_materials',
            'server' => '10.76.1.50',
            'username' => '',
            'password' => ''
        ]);
    }

    public function get_files() {
        $data = $this->database->select('files', [
            'url',
            'pdf'
        ]);

        try {
            $file = fopen('4631.txt', 'w');
            foreach ($data as $doc) {
                $pdf  = $doc['pdf'];
                $url =$doc['url'];
                fwrite($file, "$pdf\t$url\n");
            }
        } catch (Exception $e) {
            var_dump($e);
        } finally {
            fclose($file);
        }
    }

    public function generate($filePath, $url) {
        if (substr($url, -4) == '.pdf') {
            file_put_contents("$filePath", file_get_contents($url));
        } else {
            $cmd = "google-chrome --no-sandbox --headless --disable-gpu --print-to-pdf='$filePath' $url";
            exec($cmd);
        }
    }

    public function fix($pdf, $url) {
        try {
            $filePath = self::PDF_DIR . $pdf;
            echo $pdf . PHP_EOL;
            if ($pdf == 'Syphilis_1585000300.PDF') {
                echo $pdf . PHP_EOL;
            }
            if (!file_exists($filePath)) {
                $this->generate($filePath, $url);
                return;
            }

            $file  = fopen($filePath, 'r');
            $line = fgets($file);
            if (!$line || strpos($line, 'PDF') !== false) {
                return;
            }
            $this->generate($filePath, $url);
            fclose($file);
        } catch (Exception $e) {
            var_dump($e);
        }

    }
}

$foo = new AZH4631();

$file = fopen('4631.txt', 'r');
while (!feof($file)) {
    $lines = explode("\t", fgets($file));
    $pdf = trim($lines[0]);
    $url = trim($lines[1]);
    $foo->fix($pdf, $url);
}
fclose($file);





