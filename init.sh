#!/usr/bin/env bash
# Run this script after pip install

ROOT_PATH=$(pwd)
HOME_DIR=$HOME

# For Ansible
ln -nsf $ROOT_PATH/ansible.cfg $HOME_DIR/.ansible.cfg
ln -nsf $ROOT_PATH/hosts.yml $HOME_DIR/.ansible/

# For Jupyter
ln -nsf $ROOT_PATH/jupyter_notebook_config.py $HOME_DIR/.jupyter/
ln -nsf $ROOT_PATH/notebooks $HOME_DIR/.jupyter/

# For Jupytext
jupyter labextension install jupyterlab-jupytext

# For Jupyter Ipywidgets
jupyter nbextension enable --py widgetsnbextension --sys-prefix
jupyter labextension install @jupyter-widgets/jupyterlab-manager

# For hide_code
jupyter nbextension install --py hide_code --sys-prefix
jupyter nbextension enable --py hide_code --sys-prefix
jupyter serverextension enable --py hide_code