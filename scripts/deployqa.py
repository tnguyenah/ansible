from jira import JIRA
from pprint import pprint
from collections import defaultdict
from sys import platform, argv
from subprocess import PIPE, check_output, run, CalledProcessError
from termcolor import colored, cprint
import os
import paramiko
import argparse
import urllib.parse
import urllib.request
import ssl
import socket


class Issue:
    DEVELOPER_FIELD = "customfield_13604"
    JIRA_STATUS_OPEN = "Open"

    def __init__(self, jira_issue):
        self.jira_issue = jira_issue

    def get_developer(self):
        return self.jira_issue.raw['fields'][self.DEVELOPER_FIELD]

    def assign(self, account_id):
        return self.jira_issue.update(fields={'assignee': {'accountId': account_id}})


# Define constants
JIRA_STATUS_IN_QA = "In QA"
JIRA_STATUS_CODE_CONFLICT = "Code Conflict"
JIRA_STATUS_CODE_REVIEW = "Code Review"
JIRA_TRANSITION_CODE_CONFLICT = "Code Conflict"
JIRA_TRANSITION_IN_QA = "In QA"
DEFAULT_GIT_DIR_WIN = r"D:\ChartAccess_System\GIT_Clones\chartaccess"
DEFAULT_GIT_DIR_LIN = "/inetpub/git/chartaccess"
TEMP_CHART_QA = "/var/chartqa" if platform == 'linux' else r"D:\ChartAccess_System\GIT_Clones\chartqa"
SKIP_FILE = "/tmp/skip.txt" if platform == 'linux' else r"D:\ChartAccess_System\skip.txt"
QA_USERS = [
    '5c2fdfdcfbbe6428a7f34a11',  # cindy
    '5bcdf74e234047479f99f02f',  # harsh
    '557058:c1edc06c-bca6-4afc-9af0-8b7152a74ccc',  # julee
    '5b103c604bec0e53fdb541b5',  # ronda
    '5bae38dfb6a5385ea2748d4c',  # saba
    '5ca26565e935e80e41c3b912'  # vaqas
]
SERVER_NAME = {
    'hospitalqal01': 'hospitalqa',
    'IPMTWIN01': 'hospitalqa',
    'hospitaldevlin01': 'hospitaldev',
    'HOSDEVWIN01': 'hospitaldev',
}

jira = None
args = None
file_out = None

def parse_arguments():
    global args
    default_git_dir = DEFAULT_GIT_DIR_WIN if platform == "win32" else DEFAULT_GIT_DIR_LIN

    parser = argparse.ArgumentParser()
    parser.add_argument("--email", required=True, help="yourname@azaleahealth.com")
    parser.add_argument("--api-key", required=True, help="your api key")
    parser.add_argument("--qa-user", nargs="?", default="5c2fdfdcfbbe6428a7f34a11", help="set qa user")
    parser.add_argument("--win-ip", nargs="?", default="10.1.10.86", help="your windows'ip address")
    parser.add_argument("--git-dir-win", nargs="?", default=DEFAULT_GIT_DIR_WIN, help="git directory on windows")
    parser.add_argument("--git-dir-lin", nargs="?", default=DEFAULT_GIT_DIR_LIN, help="git directory on linux")
    parser.add_argument("--max-issues", nargs="?", type=int, default=1000, help="maximum tickets to be deployed")
    parser.add_argument("--ticket-list", nargs="*", default='',
                        help="list of space-delimited tickets (without AZH), e.g., --ticket-list 3456 7890"
                             + " Used --integrate to merge these tickets on top of the current qa branch")
    parser.add_argument("--only", nargs="*", default="")
    parser.add_argument("--no-comment", nargs="?", default=False, const=True, help="do not add comment to the ticket")
    parser.add_argument("--dry-run", nargs="?", default=False, const=True,
                        help="only deploy, make no change of jira ticket")
    parser.add_argument("--integrate", nargs="?", default=False, const=True, help="merge to the current qa branch")
    parser.add_argument("-v", "--verbose", nargs="?", default=False, const=True, help="show all output and error")
    parser.add_argument("--jira-server", nargs="?", default="https://simplifymd.atlassian.net")
    parser.add_argument("--git-dir", default=default_git_dir, help="/path/to/chartaccess")
    parser.add_argument("--win-deploy", nargs="?", default=False, const=True, help="perform deployment for Windows")
    args = parser.parse_args()

    setattr(args, 'stdoutV', None if args.verbose else PIPE)
    setattr(args, 'stderrV', None if args.verbose else PIPE)

    return args


def pre_cleanup():
    print(colored("Cleaning up from last time", "green"))
    trash_files = [
        ['.git', 'index.lock'],
        ['.git', 'worktrees', 'chartqa', 'index.lock']
    ]

    for file in trash_files:
        try:
            os.remove(os.path.join(*file))
        except:
            pass

    try:
        p = run(["git", "checkout", "--detach"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)

        # once there is a weird file. Clean all files to make sure no error. Don't clean on linux to avoid losing config files
        if platform == "win32":
            run(["git", "clean", "--df"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)

        if args.verbose:
            pprint(p)
    except:
        pass
    p = run(["git", "branch", "-D", "release/rc2.0_qa_tmp"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir)
    if args.verbose:
        pprint(p)


def prepare_qa_branch():
    if args.integrate:
        return
    pre_cleanup()

    print(colored("Preparing branch for merging.", "green"))
    try:
        p1 = run(["git", "reset", "--hard"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)
        p2 = run(["git", "checkout", "-b", "release/rc2.0_qa_tmp", "origin/release/rc2.0"], stdout=args.stdoutV,
                 stderr=args.stderrV, cwd=TEMP_CHART_QA)
        if args.verbose:
            pprint(p1)
            pprint(p2)
    except:
        cprint(f"Temporary work-tree not found. Trying to create a new work-tree in {TEMP_CHART_QA}", "green")
        result = run(["git", "worktree", "add", TEMP_CHART_QA, "origin/release/rc2.0", "-f"], cwd=args.git_dir)
        if result.returncode != 0:
            cprint(repr(result), "red")
            exit()

    p = run(["git", "pull"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)
    if args.verbose:
        pprint(p)


def get_all_branches(pattern="origin/release/rc2.0*[0-9]"):
    cprint('Get branch list', 'green')

    try:
        run(["git", "fetch", "--prune"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)
        branches = check_output(
            ["git", "branch", "-r", "--list", pattern],
            stderr=args.stderrV,
            universal_newlines=True,
            cwd=TEMP_CHART_QA
        )
    except CalledProcessError as err:
        exit(1)

    branches = [x.strip() for x in branches.splitlines()]
    branches.append("origin/release/rc2.0")

    return branches


def get_branch_name(issue_key, branches):
    return [x for x in branches if x.endswith(issue_key)]


def update_file_list(issue_key, branch, file_index):
    sha = check_output(["git", "merge-base", "origin/release/rc2.0", branch], universal_newlines=True).strip()
    files = check_output(["git", "diff", sha, branch, "--name-only"], universal_newlines=True).splitlines()
    for file in files:
        file_index[file].append(issue_key)


def process_merge_fail(file_index):
    out = check_output(["git", "diff", "--name-only", "--diff-filter=U"], universal_newlines=True)
    fail_files = []
    conflicting_issues = []
    for file_path in out.splitlines():
        fail_files.append(file_path)
        conflicting_issues = list(set().union(conflicting_issues, file_index[file_path]))
    return fail_files, conflicting_issues


def switch_to_qa():
    run(["git", "checkout", "--detach"], stdout=args.stdoutV, stderr=args.stderrV, cwd=TEMP_CHART_QA)
    run(["git", "checkout", "-f", "release/rc2.0_qa_tmp"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir)
    run(["git", "branch", "-D", "release/rc2.0_qa"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir)
    run(["git", "branch", "-m", "release/rc2.0_qa"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir)
    print("== (: Done :) ==")


def deploy_windows():
    print(colored("Starting Windows Deployment. Please wait!", "green"))
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.connect(args.win_ip, username="Administrator", password="1010l@m@r!")
    try:
        stdin, stdout, stderr = client.exec_command(
            f"python {args.git_dir_win}\infra\deployqa.py " + " ".join(argv[1:]),
            timeout=300,
        )

        for line in stdout:
            print("[Win] " + line.strip("\n"))
        for line in stderr:
            print("[Win] " + line.strip("\n"))
    except:
        print('Deploy Windows fail. Please run the deploy script on Windows.')
    finally:
        stdin.close()
        stdout.close()
        stderr.close()
        client.close()


def get_compared_url(current_branch, fail_issues, all_branches):
    compare_url = "https://bitbucket.org/azaleahealth/chartaccess/branches/compare/{}%0D{}#diff"
    compare_urls = []
    for fail_issue in fail_issues:
        this_branch = urllib.parse.quote_plus(current_branch.replace("origin/", ""))
        fail_branch = urllib.parse.quote_plus((get_branch_name(fail_issue, all_branches)[0]).replace("origin/", ""))
        compare_urls.append(compare_url.format(this_branch, fail_branch))
    return compare_urls

def get_required_issues():
    issues = []

    if args.only != '':
        ticket_list = ','.join(['AZH-' + x for x in args.only])
        if len(ticket_list) == 0:
            return issues
        wanted = 'project=AZH AND key in ({}) ORDER BY priority DESC, key ASC'.format(ticket_list)
        issues = jira.search_issues(wanted, maxResults=1000)
        args.max_issues = 0
    else:
        in_review = f'project=AZH AND ((status = "In Progress" AND "Testing Status" = Passed) ' \
              f'OR status IN ("{JIRA_STATUS_CODE_REVIEW}")) ' \
              f'ORDER BY priority DESC, key ASC'

        result = jira.search_issues(in_review, maxResults=100)
        while len(result) > 0:
            issues = issues + result
            result = jira.search_issues(in_review, startAt=len(issues))

        ticket_list = ','.join(['AZH-' + x for x in args.ticket_list])

        if len(ticket_list) > 0:
            wanted = 'project=AZH AND key in ({}) ORDER BY priority DESC, key ASC'.format(ticket_list)
            issues = issues + jira.search_issues(wanted)
        cprint(f"Found {len(issues)} required issues", "green")
    return issues

def get_more_issues():
    start_at = 0
    batch = 100
    ticket_list = ','.join(['AZH-' + x for x in args.ticket_list])
    issues = []
    while True:
        jql = f'project=AZH AND status IN ("{JIRA_STATUS_IN_QA}", "{JIRA_STATUS_CODE_CONFLICT}") '
        if len(ticket_list) != 0:
            jql += f'AND key NOT IN ({ticket_list}) '
        jql += 'ORDER BY type ASC, priority DESC, key ASC '

        found_issues = jira.search_issues(jql, maxResults=batch, startAt=start_at)
        issues = issues + found_issues
        if len(found_issues) == 0 or len(found_issues) < batch:
            break

        start_at = start_at + batch

    cprint(f"Found {len(issues)} additional issues", "green")
    return issues


def unassign_qa():
    def work_on_issue(issue):
        if issue.fields.assignee is None:
            return

        my_issue = Issue(issue)
        developer = my_issue.get_developer()
        if developer is not None:
            my_issue.assign(developer['accountId'])
            cprint('{}: Done'.format(issue.key), 'green')
        else:
            cprint('{}: Developer not found'.format(issue.key), 'yellow')


    global jira

    if platform != "linux":
        return

    cprint('Unassign current tickets in QA', 'green')
    try:
        jql = 'project = AZH AND status IN ("{}") AND assignee in ({})'.format(
            JIRA_STATUS_IN_QA,
            ",".join(QA_USERS)
        )
        issues = jira.search_issues(jql, maxResults=1000)
        for issue in issues:
            work_on_issue(issue)
    except Exception as e:
        pprint(e)

def get_skip_list():
    if not os.path.exists(SKIP_FILE):
        return []
    with open(SKIP_FILE) as f:
        content = f.read()
        content_list = content.splitlines()
        return content_list

def assign_qa(issue):
    if not args.dry_run and platform == "win32":
        status = issue.fields.status
        try:
            if status.name in [JIRA_STATUS_IN_QA, JIRA_STATUS_CODE_CONFLICT]:
                if issue.fields.customfield_13701 is not None and issue.fields.customfield_13701 is not []:
                    issue.update(
                        fields={'assignee': {'accountId': issue.fields.customfield_13701[0].accountId}})
                else:
                    issue.update(assignee={'accountId': args.qa_user})
            if status.name == JIRA_STATUS_CODE_CONFLICT:
                jira.transition_issue(issue, JIRA_TRANSITION_IN_QA)
        except Exception as e:
            cprint(repr(e), "red")
            pass

def merge_branch(issue, branch, all_branches, qa_counter, file_index):
    status = issue.fields.status
    my_issue = Issue(issue)
    process = run(["git", "merge", branch, "--no-edit"], stdout=args.stdoutV, stderr=args.stderrV)

    if args.verbose:
        pprint(process)

    if process.returncode == 0:
        if file_out is not None:
            file_out.write(f'Merged {issue.key} : {status.name} : {issue.fields.summary}\n')
        if status.name in [JIRA_STATUS_IN_QA, JIRA_STATUS_CODE_CONFLICT]:
            qa_counter += 1
        cprint(f"\t=> Succeeded. {qa_counter} issues In QA", "green")
        update_file_list(issue.key, branch, file_index)

        server_name = socket.gethostname()
        comment = f"Attempted to deploy to {SERVER_NAME[server_name] if server_name in SERVER_NAME else server_name} and succeeded."
        assign_qa(issue)

    else:
        cprint("\t=> Fail", "red")
        fail_files, fail_issues = process_merge_fail(file_index)
        run(["git", "merge", "--abort"])

        if len(fail_issues) == 0:
            comment = "[~{}] Please resolve conflicts. \n{}".format(
                issue.fields.customfield_13604.accountId if issue.fields.customfield_13604 is not None else '',  # developer field
                "\n".join(get_compared_url(branch, ["origin/release/rc2.0"], all_branches))
            )
            if not args.dry_run and platform == "win32":
                try:
                    if status.name != JIRA_STATUS_CODE_CONFLICT:
                        jira.transition_issue(issue, JIRA_TRANSITION_CODE_CONFLICT)
                    developer = my_issue.get_developer()
                    if developer is not None:
                        my_issue.assign(developer['accountId'])
                except Exception as e:
                    cprint(repr(e), "red")
        else:
            comment = "Attempted to deploy to hospitaldev but fail." \
                      "\nConflicting files:\n\t{} \nfound in\n\t{}\n\t{}" \
                .format(
                "\n\t".join(fail_files),
                ", ".join(fail_issues),
                "\n".join(get_compared_url(branch, fail_issues, all_branches))
            )
    return comment, qa_counter

def add_comment(issue, comment):
    if len(comment) != 0:
        if args.verbose:
            print(comment)

    if args.dry_run or args.no_comment:
        pass
    else:
        comment = "[Automated Message] " + comment
        comments = jira.comments(issue)
        last_comment = comments[-1].body if len(comments) > 0 else None
        if comment != last_comment:
            jira.add_comment(issue.key, comment)

def do_merge():
    file_index = defaultdict(list)
    available_issues = get_required_issues()
    if args.max_issues != 0:
        available_issues = available_issues + get_more_issues()

    all_branches = get_all_branches()
    os.chdir(TEMP_CHART_QA)
    qa_counter = 0
    skip_list = get_skip_list()

    for issue in available_issues:
        # Return when the number of merged ticket is satisfied
        if qa_counter != 0 and qa_counter == args.max_issues:
            return

        if issue.key in skip_list:
            cprint("\t=> Skipped", "yellow")
            continue

        branch = get_branch_name(issue.key, all_branches)
        cprint("Merging {}".format(issue.key), "green")

        if len(branch) == 0:
            cprint("\t=> Branch Not Found", "yellow")
            comment = f"No code merged. Branch not found"
            assign_qa(issue)
        elif len(branch) > 1:
            cprint("\t=> Found > 1 Branch", "yellow")
            branch.sort(reverse=True)
            comment, qa_counter = merge_branch(issue, branch[0], all_branches, qa_counter, file_index)
            comment = "Found more than one branch. Try to merge branch: {}\n".format(branch[0]) + comment
        else:
            comment, qa_counter = merge_branch(issue, branch[0], all_branches, qa_counter, file_index)

        add_comment(issue, comment)

def main():
    global jira
    global file_out
    try:
        parse_arguments()
        os.chdir(args.git_dir)
        jira = JIRA({"server": args.jira_server}, basic_auth=(args.email, args.api_key))
        prepare_qa_branch()

        if platform == 'linux':
            file_out = open('/root/deployqa.out', 'w')
            if not args.dry_run:
                unassign_qa()
        do_merge()
        switch_to_qa()

        if platform == "linux":

            os.environ["ENVIRONMENT"] = "development"
            os.environ["GIT_ROOT_DIR"] = "inetpub"

            if not args.integrate:
                print(colored("Executing stageInetpubGit.sh", "green"))
                run(["bash", "stageInetpubGit.sh"], stdout=args.stdoutV, stderr=args.stderrV)
                run(["apachectl", "-k", "graceful"], stdout=args.stdoutV, stderr=args.stderrV)
                run(["npm", "run", "build"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir + r"/nursingboard")
                run(["npm", "run", "production"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir + r"/facility_setup")
                run(["npm", "run", "production"], stdout=args.stdoutV, stderr=args.stderrV, cwd=args.git_dir + r"/flowsheets")

            if args.win_deploy:
                deploy_windows()
        else:
            run(["robocopy", args.git_dir + r"\ChartAccess\conf\bin\lib", args.git_dir + r"\ChartAccess\coldfusion\application\bin\lib", "prognosis.chartaccess.fdb.jar"])
            urllib.request.urlopen("https://localhost/chartaccess/ui/tool.cfc?method=InitFdb&token=2n8VySh3JcKl", context=ssl._create_unverified_context())
    except Exception as e:
        pprint(colored(repr(e), "red"))
    finally:
        if file_out is not None:
            file_out.close()


main()
