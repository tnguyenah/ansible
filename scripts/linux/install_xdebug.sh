#!/usr/bin/env bash

yum install -y php-devel gcc gcc-c++ autoconf automake
wget https://xdebug.org/files/xdebug-2.2.7.tgz
sudo mv xdebug-2.2.7.tgz /usr/src/xdebug-2.2.7.tgz
cd /usr/src || exit
sudo tar xfz xdebug-2.2.7.tgz
cd xdebug-2.2.7 || exit
sudo /usr/bin/phpize
sudo ./configure
sudo make

echo "
[xdebug]
zend_extension="/usr/src/xdebug-2.2.7/modules/xdebug.so"
xdebug.remote_enable=On
xdebug.remote_host=localhost
xdebug.remote_autostart=0 " >> /etc/php.ini

service httpd restart