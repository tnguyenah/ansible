#!/bin/bash

cd ~
yum install epel-release -y
curl 'https://setup.ius.io/' -o setup-ius.sh
sh setup-ius.sh
yum --enablerepo=ius install python36u -y
ln -fs /usr/bin/python36 /usr/bin/python3
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py
echo "Done"