import os
from sqlalchemy import create_engine
from pprint import pprint
from termcolor import cprint
from sshtunnel import SSHTunnelForwarder
from paramiko import SSHClient

def get_db_info(customer):
    switcher = {
        'healthmark': 'Healthmark',
        'battlemountain': 'BattleMountain',
        'beacham': 'Beacham',
        'beaver': 'Beaver',
        'dallascounty': 'DCMC',
        'dewitt': 'Dewitt',
        'eastland': 'Eastland',
        'girard': 'Girard',
        'herington': 'Herington',
        'kitcarson': 'KitCarson',
        'ladmc': 'LADMC',
        'lauderdale': 'Lauderdale',
        'madison': 'Madison',
        'milford': 'Milford',
        'morton': 'Morton',
        'pawhuska': 'Pawhuska',
        'riverbend': 'Riverbend',
        'sedan': 'Sedan',
        'shamrock': 'Shamrock',
        'westcovina': 'WCovina',
        'wills': 'Wills',
        'winkler': 'Winkler'
    }
    
    return switcher.get(customer, 'No info')


def get_server_info(server):
    switcher = {
        '10.76.1.51': {
            'ssh_username': os.environ['SSH_USERW'],
            'ssh_password': os.environ['SSH_PASSWORDW'],
            'remote_bind_address': ('127.0.0.1', 1433),
            'local_bind_address': ('0.0.0.0', 1434),
            'connection_string': os.environ['PY_PROD_MSSQL19']
        },
        '10.76.1.100': {
            'ssh_username': os.environ['SSH_USERW'],
            'ssh_password': os.environ['SSH_PASSWORDW'],
            'remote_bind_address': ('127.0.0.1', 1433),
            'local_bind_address': ('0.0.0.0', 1434),
            'connection_string': os.environ['PY_PROD_MSSQL']
        }
    }
    
    return switcher.get(server, 'No info')

def get_customers():
    try:
        engine = create_engine(os.environ['PY_PROD_MSSQL'].replace('dev', 'MasterProduct'))
        with engine.connect() as con:
            sql = "SELECT * FROM customer WHERE customer_name not in('azhdemo') ORDER BY customer_name"
            rs = con.execute(sql)
            users = rs.fetchall()

            return users
    except Exception as e:
        pprint(e)
        
def create_tunnel(server):

    server_info = get_server_info(server)
    server = SSHTunnelForwarder(
            server,
            ssh_username=server_info['ssh_username'],
            ssh_password=server_info['ssh_password'],
            remote_bind_address=server_info['remote_bind_address'],
            local_bind_address=server_info['local_bind_address'],
    )
    
    return server