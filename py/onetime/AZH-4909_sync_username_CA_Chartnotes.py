# ---
# jupyter:
#   jupytext:
#     formats: ipynb,..//..//notebooks_py//onetime//py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + hideCode=true hidePrompt=true
from sqlalchemy import create_engine
import os
from termcolor import cprint
from sshtunnel import SSHTunnelForwarder
from paramiko import SSHClient
from pprint import pprint
from IPython.display import Markdown

engine = None

def run_on(customer, func, params):
    server = SSHTunnelForwarder(
        f'{customer}.azaleahealth.com',
        ssh_username=os.environ['SSH_USER'],
        ssh_password=os.environ['SSH_PASSWORD'],
        remote_bind_address=('127.0.0.1', 3306),
        local_bind_address=('0.0.0.0', 3307),
    )
    try:
        server.start()
        func(**params)
    finally:
        server.stop()


def get_updated_users(customer, suffix):
    try:
        engine = create_engine(os.environ['PY_PROD_MSSQL'].replace('dev',customer))
        with engine.connect() as con:
            sql = f"SELECT uname FROM USERS WHERE uname like '%{suffix}'"
            cprint(sql, 'green')
            rs = con.execute(sql)
            data = rs.fetchall()
            users = [u[0] for u in data]

            return users
    except Exception as e:
        print(f'Error at {customer}')
        pprint(e)

def update_users_in_template_engine(users, suffix):
    def generate_sql(user):
        new_username = user
        old_username = user.replace(suffix, '')

        return [
            f"UPDATE blocks SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE field_data SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE fields SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE grids SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE hospitals SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE instances SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE instances_sign_contents SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE notes SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE option_grids SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE sections SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE template_sets SET created_by = '{new_username}' WHERE created_by = '{old_username}';",
            f"UPDATE templates SET created_by = '{new_username}' WHERE created_by = '{old_username}';"
        ]

    engine = create_engine('mysql+pymysql://{}:{}@localhost:3307/templateengine'.format(os.environ['SSH_USER'], os.environ['SSH_PASSWORD']))
    with engine.connect() as con:
        

        try:
            [con.execute(sql) for user in users for sql in generate_sql(user)]
        except Exception as e:
            pprint(e)
        

def check_user_in_template_engine(users, suffix):
    engine = create_engine('mysql+pymysql://{}:{}@localhost:3307/templateengine'.format(os.environ['SSH_USER'], os.environ['SSH_PASSWORD']))
    with engine.connect() as con:
        try:
            if len(users) == 0: return
            users_str = ','.join(['"{}"'.format(u.replace(suffix, '')) for u in users])
            sql = f"SELECT count(created_by) from notes WHERE created_by IN ({users_str})"
            cprint(sql, 'yellow')
            rs = con.execute(sql)
            data = rs.fetchall()
            cprint(data, 'green' if data[0][0] == 0 else 'red')
            print("\n")
        except Exception as e:
            pprint(e)



# + [markdown] hideCode=true hidePrompt=true
# # Get all changed usernames

# + hideOutput=false hidePrompt=true hideCode=false
customers = ['beacham', 'beaver', 'dewitt', 'eastland', 'girard', 'herington', 'ladmc', 'milford', 'morton', 'pawhuska', 'sedan', 'shamrock', 'wills']

users = {customer:get_updated_users(customer, '[_]' + customer) for customer in customers}
pprint(users)
# + [markdown] hidePrompt=true hideCode=true
# # Check if the username is updated?

# + hideOutput=false hidePrompt=true hideCode=false
for customer in customers:
    run_on(customer, check_user_in_template_engine, {'users':users[customer], 'suffix':f'_{customer}'})


# + [markdown] hidePrompt=true hideCode=true
# # Run update

# + hidePrompt=false hideCode=true
for customer in customers:
    if customer == 'beacham': continue
    cprint(f'Update for {customer}')
    run_on(customer, update_users_in_template_engine, {'users':users[customer], 'suffix':f'_{customer}'})

# + [markdown] hidePrompt=true hideCode=true
# # Check if the username is updated again

# + hidePrompt=true
for customer in customers:
    run_on(customer, check_user_in_template_engine, {'users':users[customer], 'suffix':f'_{customer}'})
# -


