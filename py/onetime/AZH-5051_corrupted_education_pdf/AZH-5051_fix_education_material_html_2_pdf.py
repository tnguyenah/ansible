# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import os
import sys

module_path = os.path.abspath(os.path.join('..', '..', 'lib'))
if module_path not in sys.path:
    sys.path.append(module_path)

import common as mylib
import shutil
from IPython.display import display, Markdown
from ipywidgets import *
from sqlalchemy import create_engine
from pprint import pprint
from pathlib import Path
import weasyprint


# +
def get_file_path(customer):
    return f'/mnt/cifs/azhcustomershare/{customer}/patient_files/ChartExport'


def save_url_to_file(accordion):
    try:
        url_dict = read_url_from_file()

        for vbox in accordion.children:
            pattern = vbox.children[0].value
            url = vbox.children[1].value
            if len(url) == 0:
                continue
            url_dict[pattern] = url

        f = open('url.txt', 'w')
        for pattern in sorted(url_dict.keys()):
            f.write(f'{pattern}\t{url_dict[pattern]}\n')
    finally:
        f.close()


def read_url_from_file():
    url_dict = {}
    try:
        f = open('url.txt', 'r')
        for line in f:
            cols = line.strip().split('\t')
            url_dict[cols[0]] = cols[1]
    finally:
        f.close()
    return url_dict


def get_suspected_files(customer):
    customers20 = [x[1] for x in mylib.get_customers()]
    server_info = mylib.get_server_info('10.76.1.51') if customer in ['healthmark', 'battlemountain',
                                                                      'heights'] else mylib.get_server_info(
        '10.76.1.100')
    engine = create_engine(server_info['connection_string'].replace('dev', mylib.get_db_info(customer)))
    with engine.connect() as con:
        sql = "select file_name from files where FILE_TYPE_ID = 51 and File_path like 'ChartExport\%' and DT_LAST_UPDATED_TIME >= '2019-08-14' and FILE_EXT = 'pdf' ORDER BY file_name"
        rs = con.execute(sql)
        files = rs.fetchall()
        return files


def get_corrupted_files(customer, files):
    wrong_file_dict = {}
    wrong_file = []
    pre_file_pattern = None
    file_pattern = None
    pre_result = None
    file_path = get_file_path(customer)
    for file in files:
        file_name = file[0] + '.pdf'
        index = file_name.find('_')

        file_pattern = file_name[:index] if index > -1 else file_name

        if pre_file_pattern is None:
            pre_file_pattern = file_pattern
            continue
        if file_pattern == pre_file_pattern:
            if pre_result == False:
                wrong_file.append(file_name)
        else:
            if len(wrong_file) > 0:
                wrong_file_dict[pre_file_pattern] = wrong_file
            wrong_file = list()
            result = check_file(f'{file_path}/{file_name}')
            if not result:
                wrong_file.append(file_name)
            pre_result = result
            pre_file_pattern = file_pattern

    return wrong_file_dict


def check_file(file_path):
    f = None

    is_correct = True
    try:
        f = open(file_path, 'r', encoding="latin-1")
        if '%PDF' not in f.readline():
            is_correct = False
    except UnicodeDecodeError as e:
        print(e)
        pass
    except Exception as e:
        print(e)
    finally:
        if f is not None:
            f.close()
    return is_correct


def generate_content(customer, corrupted_files):
    url_dict = read_url_from_file()
    file_path = get_file_path(customer)

    def get_title(pattern, file_name):
        return url_dict[pattern] if pattern in url_dict else ''

    def prepare_html(pattern, file_name):
        f = open(f'{file_path}/{file_name}', 'r', encoding="latin-1")
        html = f.read()
        f.close()
        url = url_dict[pattern] if pattern in url_dict else ''

        return VBox([Label(pattern), Text(placeholder='Place the correct url here', value=url), HTML(html)])

    htmls = [prepare_html(pattern, files[0]) for (pattern, files) in corrupted_files.items()]
    accordion = Accordion(children=htmls, selected_index=None)

    for i, vbox in enumerate(accordion.children):
        title = vbox.children[0].value
        url = url_dict[title] if title in url_dict else ''
        accordion.set_title(i, url)

    return accordion


def fix_corrupted_files(customer, corrupted_files, accordion):
    import pdfkit

    save_url_to_file(accordion)
    url_dict = read_url_from_file()
    options = {
        'quiet': '',
        'page-size': 'Letter',
        'user-style-sheet': 'style.css',
        'print-media-type': None,
        'disable-smart-shrinking': None
    }

    Path(f'out/{customer}').mkdir(parents=True, exist_ok=True)
    for (pattern, files) in corrupted_files.items():
        for file in files:
            try:
                #                 Use for sites: niddk.nih.gov
                pdfkit.from_url(url_dict[pattern], f'out/{customer}/' + file, options=options)
            #                 Use for sites: ghr.nlm.nih.gov
            #                 weasyprint.HTML(url_dict[pattern]).write_pdf(f'out/{customer}/' + file)
            except Exception as e:
                print(f'Fail: {file}')
                pprint(e)
                continue


def replace_corrupted_files(customer):
    src = os.path.abspath(f'out/{customer}')
    dest = os.path.join(get_file_path(customer))

    files = os.listdir(src)
    for file in files:
        shutil.move(os.path.join(src, file), os.path.join(dest, file))


# -

# # Healthmark

# +
customer = 'healthmark'
with mylib.create_tunnel('10.76.1.51'):
    suspected_files = get_suspected_files(customer)
    corrupted_files = get_corrupted_files(customer, suspected_files)

print('Corrupted files start with:') if len(corrupted_files) > 0 else print('No corrupted file found!')
for (k, v) in corrupted_files.items():
    print(f'{k}: {len(v)}')

accordion = generate_content(customer, corrupted_files)
display(accordion)
# -

fix_corrupted_files(customer, corrupted_files, accordion)

replace_corrupted_files(customer)

customers = ['battlemountain', 'heights', 'healthmark']
for customer in customers:
    display(Markdown(f'# Working on {customer}'))
    with mylib.create_tunnel('10.76.1.51'):
        suspected_files = get_suspected_files(customer)
        corrupted_files = get_corrupted_files(customer, suspected_files)
    print('Corrupted files start with:') if len(corrupted_files) > 0 else print('No corrupted file found!')
    for (k, v) in corrupted_files.items():
        print(f'{k}: {len(v)}')

    accordion = generate_content(customer, corrupted_files)
    display(accordion)

info_dict = {}


def run_4_customer(customer):
    def on_click_fix(change):
        notification.value = 'Fixing'
        cust = change.description.replace('Fix ', '')
        fix_corrupted_files(cust, info_dict[cust]['corrupted_files'], info_dict[cust]['accordion'])
        notification.value = 'Done'

    def on_click_replace(change):
        notification.value = 'Replacing'
        cust = change.description.replace('Replace ', '')
        replace_corrupted_files(cust)
        notification.value = 'Done'

    display(Markdown(f'# Working on {customer}'))
    suspected_files = get_suspected_files(customer)
    corrupted_files = get_corrupted_files(customer, suspected_files)

    if len(corrupted_files) == 0:
        print('No corrupted file found!')
    else:
        print('Corrupted files start with:')
        for (k, v) in corrupted_files.items():
            print(f'{k}: {len(v)}')

        accordion = generate_content(customer, corrupted_files)
        fix_button = Button(description=f'Fix {customer}', button_style='info')
        fix_button.on_click(on_click_fix)
        replace_button = Button(description=f'Replace {customer}', button_style='info')
        replace_button.on_click(on_click_replace)
        notification = Label(value="")

        info_dict[customer] = {'corrupted_files': corrupted_files, 'accordion': accordion}
        display(accordion)
        display(fix_button)
        display(replace_button)
        display(notification)


# customers = [x[1] for x in mylib.get_customers()]
customers = ['winkler']
for customer in customers:
    run_4_customer(customer)

# # Fix 1 file

# +
import pdfkit
from weasyprint import HTML

url_dict = read_url_from_file()
customer = 'beacham'
options = {
    'quiet': '',
    'page-size': 'Letter',
    'user-style-sheet': 'style.css',
    'print-media-type': None,
    'disable-smart-shrinking': None
}
pattern = 'High Blood Pressure(en)'
file = 'High Blood Pressure(en)_46259_115_10021_20191209_123713738_1.PDF'
Path(f'out/{customer}').mkdir(parents=True, exist_ok=True)

HTML(url_dict[pattern]).write_pdf(f'out/{customer}/' + file)
# pdfkit.from_url(url_dict[pattern], f'out/{customer}/' + file, options=options)
# -

replace_corrupted_files(customer)
