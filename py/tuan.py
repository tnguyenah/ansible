# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + tags=["hide_input"]
from pprint import pprint
from IPython.display import Markdown
import os

email = os.environ['YOUR_MAIL'] if 'YOUR_MAIL' in os.environ else 'PUT YOUR EMAIL HERE'
api_key = os.environ['YOUR_API'] if 'YOUR_API' in os.environ else 'PUT YOUR API KEY HERE'


# + [markdown] hidePrompt=true
# # Unset Assignee field and set Assigned Tester field

# + tags=["hide_input"] hidePrompt=true hideCode=true
from jira import JIRA

def work_on_issue(issue):
    if issue.fields.assignee is None:
        return

    issue.update(fields={'customfield_13701': [{'accountId': issue.fields.assignee.accountId}]})
    issue.update(fields={'assignee': {'accountId': None}})
    print('{}: Done'.format(issue.key))
    

try:
    jira = JIRA({'server': 'https://simplifymd.atlassian.net'}, basic_auth=(email, api_key), max_retries=0)
    jql = 'project = AZH AND status = "In QA" AND assignee is not EMPTY'
    issues = jira.search_issues(jql, maxResults=1000)
    for issue in issues:
        pass
#         work_on_issue(issue)
except Exception as e:
    pprint(e)
# -
# # Change Testing Users

# + tags=["hide_input"]
from jira import JIRA

def work_on_issue(issue):
    print('{}'.format(issue.key))
    if issue.fields.customfield_13701 is not None:
        pprint(issue.fields.customfield_13701[0])
        if issue.fields.customfield_13701[0].accountId != '5c2fdfdcfbbe6428a7f34a11':
            issue.update(fields={'customfield_13701': [{'accountId': '5c2fdfdcfbbe6428a7f34a11'}]})

try:
    jira = JIRA({'server': 'https://simplifymd.atlassian.net'}, basic_auth=(email, api_key), max_retries=0)
    jql = 'project = AZH AND status = "In QA"'
    issues = jira.search_issues(jql, maxResults=1000)
    for issue in issues:
        pass
#         work_on_issue(issue)
except Exception as e:
    pprint(e)
# -


